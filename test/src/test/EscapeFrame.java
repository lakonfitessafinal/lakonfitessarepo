package test;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class EscapeFrame extends JFrame {
   public EscapeFrame() {
      super("SuperFrame");
      this.setContentPane(new JPanel() {
                              public void paintComponent(Graphics g) {
                                 g.setFont(new Font("Serif", Font.PLAIN, 18));
                                 g.drawString("Please press ESC to quit...", 10, 50);
                              }
                          });
     this.addKeyListener(new ExternalEscKeyListener(this));
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.setSize(300, 200);
      this.setLocation(200, 100);
      this.setVisible(true);
   }
   
   public static void main(String[] args) {
       new EscapeFrame();
   }
}

